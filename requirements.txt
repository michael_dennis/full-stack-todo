aniso8601==7.0.0
asgiref==3.2.10
dateparser==0.7.6
Django==3.1.1
djangorestframework==3.11.1
graphene==2.1.8
graphene-django==2.13.0
graphql-core==2.3.2
graphql-relay==2.0.1
pip==20.1.1
promise==2.3
python-dateutil==2.8.1
pytz==2020.1
regex==2020.7.14
Rx==1.6.1
setuptools==47.1.0
singledispatch==3.4.0.3
six==1.15.0
sqlparse==0.3.1
tzlocal==2.1
Unidecode==1.1.1