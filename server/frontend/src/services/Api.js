import Cookies from 'js-cookie';

import { ApolloClient, createHttpLink, InMemoryCache, gql } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink({
  uri: '/graphql/',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = Cookies.get('csrftoken')
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      'X-CSRFToken': token ? token : null
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});


export async function getTodos(userId=null, statusId=null, dueBy=null, dueAfter=null) {

  let filters = []
  let filter_str = ''
  if (userId) {
    filters.push(`userId: ${userId}`)
  }
  if (statusId) {
    filters.push(`statusId: ${statusId}`)
  }
  if (dueBy) {
    filters.push(`dueBy: "${dueBy}"`)
  }
  if (dueAfter) {
    filters.push(`dueAfter: "${dueAfter}"`)
  }
  if (filters.length > 0) {
    filter_str = `(${filters.join(', ')})`;
  }
  return client
    .query({
      query: gql`
        query getTodos {
          todos${filter_str} {
            id
            title
            description
            dueDate
            user {
              id
              firstName
              lastName
            }
            status {
              id
              title
            }
          }
        }
        `
    })
}

export function getStatuses() {
  client
    .query({
      query: gql`
        query getStatuses {
          statuses {
            id
            title
          }
        }
        `
    }).then(result => console.log(result))
}

export function getUsers() {
  client
    .query({
      query: gql`
        query getUsers {
          users {
            id
            firstName
            lastName
          }
        }
        `
    }).then(result => console.log(result))
}