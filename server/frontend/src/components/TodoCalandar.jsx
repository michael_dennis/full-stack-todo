import React, { useState } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'

const localizer = momentLocalizer(moment)

function TodoCalendar({todos, activeIndex, setActiveIndex }) {
    const [calandarEvents] = useState(getCalandarEventsFromTodos())

    function getCalandarEventsFromTodos() {
        let events = []
        todos.map((todo) => {
            if (todo.dueDate) {
                const event = {
                    title: todo.title,
                    start: Date.parse(todo.dueDate),
                    end: Date.parse(todo.dueDate),
                    allDay: true,
                    index: todo.index
                }
                events.push(event);
            }
        });
        return events
    }
 
    return(
        <div className='calandar-view'>
            <Calendar
                localizer={localizer}
                events={calandarEvents}
                startAccessor="start"
                endAccessor="end"
                style={{ height: "100%" }}
                onSelectEvent={(event) => { setActiveIndex(event.index) }}/>
        </div>
    )
}

export default TodoCalendar;