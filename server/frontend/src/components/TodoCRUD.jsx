import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import { generateUUID } from '../services/Utils.js';

Modal.setAppElement('#app')


function TodoCRUD({ modalIsOpen=false, afterOpenModal, closeModal }) {

    const customStyles = {
        content : {
          top                   : '50%',
          left                  : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)'
          
        }
      };
 
      
    return (
        <Modal
          className='modal'
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <h2>Hello</h2>
          <button onClick={closeModal}>close</button>
          <div>I am a modal</div>
          <form>
            <input />
            <button>tab navigation</button>
            <button>stays</button>
            <button>inside</button>
            <button>the modal</button>
          </form>
        </Modal>
    )
}

export default TodoCRUD;