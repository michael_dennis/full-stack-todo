import React, { useState } from 'react';
import TodoListItem from './TodoListItem.jsx';
import { generateUUID } from '../services/Utils.js';

function TodoDetails({ todo, openModal },) {

    return (
        <div className='todo-details'>
            <h4>{todo.title}</h4>
            {
                todo.description ?
                    <p><b>Description: </b>{todo.description}</p>
                    :
                    null
            }
            {
                todo.dueDate ?
                    <p><b>Due Date: </b>{todo.dueDate}</p>
                    :
                    null
            }
            {
                todo.user && todo.user.firstName && todo.user.lastName ?
                    <p><b>User: </b>{todo.user.firstName} {todo.user.lastName}</p>
                    :
                    null
            }           
            {
                todo.status ?
                    <p><b>Status: </b>{todo.status.title}</p>
                    :
                    null
            }

            <input type="button" value="Edit Todo" onClick={openModal} />
        </div>
    )
}
 
export default TodoDetails;