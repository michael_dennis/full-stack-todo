import React, { useEffect } from 'react';

function TodoListItem({ todo, focused, todoSelected, id }) {

    return (
        <li id={id} className={`todo ${focused ? 'focused' : ''}`} key={todo.id} onClick={() => { todoSelected(todo.index) }}>
            <div>
                <span className='title'>{todo.title}</span>
            </div>
            
        </li>
    )
}

export default TodoListItem; 