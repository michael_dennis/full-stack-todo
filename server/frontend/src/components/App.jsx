import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import { getTodos } from "../services/Api";
import SplitPane, { Pane } from 'react-split-pane';
import Modal from 'react-modal';
import TodoList from "./TodoList.jsx";
import TodoDetails from "./TodoDetails.jsx";
import TodoCalendar from "./TodoCalandar.jsx";

import style from "../assets/scss/main.scss";


const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

function App() {
  var subtitle;
  const [todos, setTodos] = useState();
  const [activeIndex, setActiveIndex] = useState(0);
  const [modalIsOpen,setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }
 
  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = '#f00';
  }
 
  function closeModal(){
    setIsOpen(false);
  }

  async function initData()  {
    const todos_response = await getTodos();
    let todos = []
    todos_response.data.todos.map((todo, index) => {
      todos.push({...todo, index});
    });
    setTodos(todos);
    console.log(todos);
  }

  useEffect(() => {
    initData()
  }, []);

  return (

    <div className='page main'>
      {
        todos ?
          <div style={{ height: "100%" }}>
            <SplitPane className='split-pane' split="horizontal">
              <Pane className='list-pane' initialSize="25%">
                  <TodoList id='todo-list' todos={todos} activeIndex={activeIndex} setActiveIndex={setActiveIndex} />
              </Pane>
              
              <Pane className='details-pane'>
                <TodoDetails todo={todos[activeIndex]} openModal={openModal}/>
              </Pane>
            </SplitPane>
            <TodoCalendar todos={todos} activeIndex={activeIndex} setActiveIndex={setActiveIndex}/>
          </div>
          :
          <p>loading...</p>
      }
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
 
          <h2 ref={_subtitle => (subtitle = _subtitle)}>Hello</h2>
          <button onClick={closeModal}>close</button>
          <div>I am a modal</div>
          <form>
            <input />
            <button>tab navigation</button>
            <button>stays</button>
            <button>inside</button>
            <button>the modal</button>
          </form>
        </Modal>
    </div>
  )
}
// export default App;

const container = document.getElementById("app");
render(<App />, container);