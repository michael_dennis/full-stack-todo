import React, { useState, useEffect } from 'react';
import TodoListItem from './TodoListItem.jsx';
import { generateUUID } from '../services/Utils.js';

function TodoList({ todos, activeIndex, setActiveIndex }) {
    
    useEffect(() => {
        const activeElementId = `li-${activeIndex}`;
        let scrollableElement = document.getElementsByClassName('list-pane')[0];
        let activeElement = document.getElementById(activeElementId);
        
        if (scrollableElement && activeElement) {
            const topPos = activeElement.offsetTop;
            scrollableElement.scrollTop = topPos;
        }
    }, [activeIndex])

    return (
        <div>
            <ul className='todo-list'>
                {todos.map((todo) => 
                    <TodoListItem
                        id={`li-${todo.index}`}
                        todo={todo} 
                        key={generateUUID()}
                        todoSelected={setActiveIndex}
                        focused={todo.index===activeIndex}/>
                )}
            </ul>
        </div>
    )
}

export default TodoList;