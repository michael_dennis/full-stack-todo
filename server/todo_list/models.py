from django.db import models

class User(models.Model):
    first_name = models.CharField(max_length=25, null=False, blank=False)
    last_name = models.CharField(max_length=25, null=False, blank=False)

class Status(models.Model):
    title = models.CharField(max_length=14, null=False, blank=False)

class Todo(models.Model):
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, unique=False)
    title = models.CharField(max_length=100, null=False, blank=False)
    description = models.TextField(null=True, blank=True)
    status = models.ForeignKey(Status, null=True, blank=True, on_delete=models.SET_NULL, unique=False)
    due_date = models.DateTimeField(null=True, blank=True)
