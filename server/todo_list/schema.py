import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from server.todo_list.models import User, Status, Todo
from datetime import datetime
from django.db.models import Q
import dateparser

class UserType(DjangoObjectType):
    class Meta:
        model = User

class StatusType(DjangoObjectType):
    class Meta:
        model = Status

class TodoType(DjangoObjectType):
    class Meta:
        model = Todo

# Create a Query type
class Query(ObjectType):
    user = graphene.Field(UserType, id=graphene.Int())
    status = graphene.Field(StatusType, id=graphene.Int())
    todo = graphene.Field(TodoType, id=graphene.Int())

    users = graphene.List(UserType)
    statuses = graphene.List(StatusType)
    todos = graphene.List(TodoType, user_id=graphene.Int(), 
                            status_id=graphene.Int(), due_by=graphene.String(), 
                            due_after=graphene.String())

    def resolve_user(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return User.objects.get(pk=id)

        return None

    def resolve_status(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Status.objects.get(pk=id)

        return None

    def resolve_todo(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Todo.objects.get(pk=id)

        return None

    def resolve_users(self, info, **kwargs):
        return User.objects.all()

    def resolve_statuses(self, info, **kwargs):
        return Status.objects.all()

    def resolve_todos(self, info, **kwargs):
        user_id = kwargs.get('user_id', None)
        status_id = kwargs.get('status_id', None)
        due_by = kwargs.get('due_by', None)
        due_after = kwargs.get('due_after', None)
        filters = []
        try:
            if user_id:
                filters.append(Q(user=user_id) | Q(user=None))
            if status_id:
                filters.append(Q(status=status_id))
            if due_by:
                filters.append(Q(due_date__lte=dateparser.parse(due_by)))
            if due_after:
                filters.append(Q(due_date__gte=dateparser.parse(due_after)))
            
            if len(filters) > 0:
                query_filter = None
                for filter in filters:
                    if not query_filter:
                        query_filter = filter
                    else:
                        query_filter = query_filter & filter
                return Todo.objects.filter(query_filter)
            else:
                return Todo.objects.all()
        except Exception as ex:
            print(ex)
            return None
        

############

class UserInput(graphene.InputObjectType):
    id = graphene.ID()
    first_name = graphene.String()
    last_name = graphene.String()

class StatusInput(graphene.InputObjectType):
    id = graphene.ID()
    title = graphene.String()

class TodoInput(graphene.InputObjectType):
    id = graphene.ID()
    title = graphene.String()
    description = graphene.String()
    user_id = graphene.ID()
    status_id = graphene.ID()
    due_date = graphene.String()


#############
class CreateUser(graphene.Mutation):
    class Arguments:
        input = UserInput(required=True)

    user = graphene.Field(UserType)

    @staticmethod
    def mutate(root, info, input=None):
        user_instance = User(first_name=input.first_name, last_name=input.last_name)
        user_instance.save()
        return CreateUser(user=user_instance)

class UpdateUser(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = UserInput(required=True)
    user = graphene.Field(UserType)

    @staticmethod
    def mutate(root, info, id, input=None):
        user_instance = User.objects.get(pk=id)
        if user_instance:
            user_instance.first_name = input.first_name
            user_instance.last_name = input.last_name
            user_instance.save()
            return UpdateUser(user=user_instance)
        return UpdateUser(user=None)


class CreateStatus(graphene.Mutation):
    class Arguments:
        input = StatusInput(required=True)

    status = graphene.Field(StatusType)

    @staticmethod
    def mutate(root, info, input=None):
        status_instance = Status(title=input.title)
        status_instance.save()
        return CreateStatus(status=status_instance)

class UpdateStatus(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = UserInput(required=True)
    status = graphene.Field(StatusType)

    @staticmethod
    def mutate(root, info, id, input=None):
        status_instance = Status.objects.get(pk=id)
        if status_instance:
            status_instance.title = input.title
            status_instance.save()
            return UpdateStatus(status=status_instance)
        return UpdateStatus(status=None)


class CreateTodo(graphene.Mutation):
    class Arguments:
        input = TodoInput(required=True)

    todo = graphene.Field(TodoType)

    @staticmethod
    def mutate(root, info, input=None):
        status_instance = Status.objects.get(pk=input.status_id) if input.status_id and Status.objects.filter(pk=input.status_id).exists() else None
        user_instance = User.objects.get(pk=input.user_id) if input.user_id and User.objects.filter(pk=input.user_id).exists() else None
        try:
            if input.due_date:
                due_date = dateparser.parse(input.due_date, settings={'TIMEZONE': 'US/Eastern'})
            else:
                due_date = None
        except Exception as ex:
            print(ex)
            return ex

        todo_instance = Todo(title=input.title, description=input.description,
                             due_date=due_date, status=status_instance, user=user_instance)

        todo_instance.save()
        return CreateTodo(todo=todo_instance)

class UpdateTodo(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = TodoInput(required=True)

    todo = graphene.Field(TodoType)

    @staticmethod
    def mutate(root, info, id, input=None):
        todo_instance = Todo.objects.get(pk=id)
        if todo_instance:

            status_instance = Status.objects.get(pk=input.status_id) if input.status_id and Status.objects.filter(pk=input.status_id).exists() else None
            user_instance = User.objects.get(pk=input.user_id) if input.user_id and User.objects.filter(pk=input.user_id).exists() else None
            
            try:
                due_date = dateparser.parse(input.due_date, settings={'TIMEZONE': 'US/Eastern'})
            except Exception as ex:
                print(ex)
                due_date = None

            todo_instance.title = input.title
            todo_instance.description = input.description
            todo_instance.user = user_instance
            todo_instance.status = status_instance
            todo_instance.due_date = due_date

            todo_instance.save()

            return UpdateTodo(todo=todo_instance)
        return UpdateTodo(todo=None)

class Mutation(graphene.ObjectType):
    
    create_user = CreateUser.Field()
    update_user = UpdateUser.Field()
    create_status = CreateStatus.Field()
    update_status = UpdateStatus.Field()
    create_todo = CreateTodo.Field()
    update_todo = UpdateTodo.Field()



schema = graphene.Schema(query=Query, mutation=Mutation)
