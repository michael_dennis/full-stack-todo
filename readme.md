# Django React Todo List

![](screenshot.png)


Create Migration Files
```
python3 manage.py makemigrations

```
Write Migrations To DB
```
python3 manage.py migrate

```
Import Test Data

```
python3 manage.py loaddata initial_data.json

```
Start Django Server

```
python3 manage.py runserver

```
frontend build
```
cd server/frontend && npm run dev

```

frontend dev auto build

```
npm run watch

```